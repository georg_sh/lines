package game.lazystudio.lines;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Rect;
import android.util.Log;


public class GameObjects
	{
	LinesGame appclass;
	public float scrw = 0;
	public float scrh = 0;
	Controller control;
	int gamehardness = Defs.HEASY;
	MyActivity activs[];
	MyActivity curr_activ;
	MyActivity prev_activ;
	Bitmap background;
	int changelevel = 0;
	Paint ch_paint;
	Rect rt;
	
	public GameObjects(LinesGame appclass, int scrw, int scrh)
		{
		this.scrw = scrw;
		this.scrh = scrh;
		rt = new Rect(0, 0, scrw, scrh);
		this.appclass = appclass;
		control = new Controller(this);
		
		ch_paint = new Paint();
		ch_paint.setStyle(Style.FILL);
		ch_paint.setColor(Color.BLACK);
		ch_paint.setAlpha(0);
		Resources res = appclass.getResources();
		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inScaled = false;
		background = BitmapFactory.decodeResource(res, R.drawable.texture1, options);

			int newwidth = background.getWidth();
			int newheight = background.getHeight();
			if (scrw < scrh)
				newwidth = newheight*scrw/scrh;
			else
				newheight = newwidth*scrh/scrw;
			background = Bitmap.createBitmap(background, 0, 0, newwidth, newheight);
			background = Bitmap.createScaledBitmap(background, scrw, scrh, false);

		activs = new MyActivity[3];
		activs[0] = new MainGame(appclass, this, scrw, scrh);
		activs[1] = new MenuClass(appclass, this, scrw, scrh);
		activs[2] = new OptionsClass(this, scrw, scrh);
		curr_activ = activs[1];
		}

public boolean Back()
	{
	if (curr_activ != activs[1]) { Resume(); ChangeAct(1, 0); return true; }
	return false;
	}
	
public void ChangeAct(int act_num, int param)
	{
	Log.d("LinesGame", "ChangeAct: "+act_num);
	prev_activ = curr_activ;
	curr_activ = activs[act_num];
	curr_activ.Resume(param);
	changelevel = 100;
	}
	
public void ControllerMove (float dx, float dy, float size)
	{
	//Log.d("LinesGame", "Move: "+dx+", "+dy);
	curr_activ.ControllerMove(dx, dy, size);
	}

public void ControllerTouch (float mx, float my)
	{
	curr_activ.ControllerTouch(mx, my);
    }

public boolean Pause()
	{
	if (changelevel > 0) { return false; }
	LinesGame.getGameView().Pause();
	return true;
	}

public boolean Resume()
	{
	LinesGame.getGameView().Resume();
	return true;
	}

public void Draw (Canvas canvas)
	{
	canvas.drawBitmap(background, 0, 0, null);
	if (changelevel > 0)
		{
		changelevel -= 2;
		if (changelevel >= 50)
			{
			prev_activ.Draw(canvas);
			int al_val = (100-changelevel)*255/50;
			Log.d("LinesGame", "alphachange1 = " + al_val);
			ch_paint.setAlpha(al_val);
			//canvas.drawRect(rt, ch_paint);
			canvas.drawBitmap(background, 0, 0, ch_paint);
			}
		else
			{
			curr_activ.Draw(canvas);
			int al_val = (changelevel)*255/50;
			Log.d("LinesGame", "alphachange2 = " + al_val);
			ch_paint.setAlpha(al_val);
			//canvas.drawRect(rt, ch_paint);
			canvas.drawBitmap(background, 0, 0, ch_paint);
			}
		}
	else
		{
		curr_activ.Draw(canvas);
		}
	}
}