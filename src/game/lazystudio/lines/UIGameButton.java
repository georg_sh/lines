package game.lazystudio.lines;

import android.graphics.Canvas;
import android.graphics.Paint;

public class UIGameButton
	{
	//Bitmap Btnb;
	int cx;
	int cy;
	int halfwidth;
	int halfheight;
	String text;
	int textw;
	int texth;
	Paint tpaint;
	
	public UIGameButton(/*Bitmap btn, */int width, int height, int cx, int cy, String text, Paint tpaint)
		{
		//Btnb = btn;
		this.cx = cx;
		this.cy = cy;
		this.halfwidth = width/2;
		this.halfheight = height/2;
		this.text = text;
		this.tpaint = tpaint;
		textw = (int)tpaint.measureText(text);
		texth = (int)tpaint.getTextSize()/2;
		}
	boolean onClick(int x, int y)
		{
		if (x < cx-halfwidth  || x > cx+halfwidth)  { return false; }
		if (y < cy-halfheight || y > cy+halfheight) { return false; }
		return true;
		}
	void DrawThis (Canvas canvas)
		{
		//canvas.drawBitmap(Btnb, cx-halfwidth, cy-halfheight, null);
		canvas.drawText(text, cx-textw/2, cy+texth-5, tpaint);
		}
	}
