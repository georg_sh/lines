package game.lazystudio.lines;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;

public class MainActivity extends Activity implements OnTouchListener
	{
	GameObjects gameobj;
	@Override
	protected void onCreate(Bundle savedInstanceState)
		{
		super.onCreate(savedInstanceState);
		Log.d("LinesGame", "SetActivity");
		LinesGame.getInstance().SetActivity(this);
		LinesGame.getGameView().setOnTouchListener(this);
		gameobj = LinesGame.getGameObj();
		}
	@Override
	protected void onPause()
		{
		
		LinesGame.getGameView().Pause();
		super.onPause();
		}
	@Override
	protected void onResume()
		{
		
		LinesGame.getGameView().Resume();
		super.onResume();
		}
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event)
		{
	    if (keyCode == KeyEvent.KEYCODE_BACK)
	    	{
	        if (gameobj.control.OnBack()) { return true; }
	    	}
	    return super.onKeyDown(keyCode, event);
		}
	@Override
	public boolean onTouch(View v, MotionEvent event)
		{
		float x = event.getX();
		float y = event.getY();
		switch (event.getAction())
			{
			case MotionEvent.ACTION_DOWN:
				{
				gameobj.control.OnDown(x, y);
				break;
				}
			case MotionEvent.ACTION_MOVE:
				{
				gameobj.control.OnMove(x, y);
				break;
				}
			case MotionEvent.ACTION_UP:
				{
				gameobj.control.OnUp(x, y);
				break;
				}
			}
		return true;
		}
	}
