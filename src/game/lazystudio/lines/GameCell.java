package game.lazystudio.lines;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;

public class GameCell
	{
	int realstate;
    Paint bcolor;
    int opacity;
    float radius;
    int albl;
    int state;
	public GameCell(float radius)
		{
		realstate = 0;
		bcolor = null;
		opacity = 100;
		this.radius = radius;
		albl = 0;
		state = 0;
		// TODO Auto-generated constructor stub
		}
	public void DrawThis(Canvas canvas, Bitmap sphere, float x, float y)
		{
		if (bcolor == null) return;
		canvas.drawBitmap(sphere, x-radius, y-radius, bcolor);
		}
	public void DrawThisEx(Canvas canvas, Bitmap sphere, float x, float y, Paint p)
		{
		if (bcolor == null) return;
		int w = sphere.getWidth();
		int h = sphere.getHeight();
		if (w/2 != (int)radius)
			{
			Rect src =  new Rect(0,0,w-1, h-1);
			Rect dest = new Rect((int)(x-radius),(int)(y-radius), (int)(x+radius), (int)(y+radius));
			canvas.drawBitmap(sphere, src, dest, p);
			}
		else
			{
			canvas.drawBitmap(sphere, x-radius, y-radius, p);
			}
		}
	}
