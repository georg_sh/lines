package game.lazystudio.lines;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.util.Log;

public class OptionsClass extends MyActivity implements DialogInterface.OnClickListener
	{
	Paint text_paint;
	Paint btnt_paint;
	AlertDialog dialog;
	UIGameButton difficulcy;
	int scrw;
	int scrh;
	GameObjects gameclass;
	public OptionsClass(GameObjects gameclass, int scrw, int scrh)
		{
		this.scrw = scrw;
		this.scrh = scrh;
		this.gameclass = gameclass;
		
		AlertDialog.Builder builder = new AlertDialog.Builder(LinesGame.GetActivity());
		builder.setTitle("Choose difficulty");
		builder.setCancelable(true);
		//builder.setPositiveButton("okay?", this);
		String items[] = {"easy", "medium", "hard"};
		builder.setItems(items, this);
		dialog = builder.create();
		
		
		btnt_paint = new Paint();
		btnt_paint.setColor(Color.WHITE);
		btnt_paint.setStyle(Style.FILL);
		btnt_paint.setTextSize(scrw/10);
		
		String difficulcy_t = "DIFFICULCY: medium";
		
		int tw = (int)btnt_paint.measureText(difficulcy_t);
		text_paint = new Paint();
		text_paint.setColor(Color.WHITE);
		//text_paint.setStyle(Style.FILL);
		text_paint.setTextSize(scrw/10);
		int btnw = (int)((float)tw*1.3);
		int btnh = scrw/7+10;
		difficulcy = new UIGameButton(btnw, btnh, scrw/2, scrh/2, difficulcy_t, btnt_paint);
		difficulcy.text = "DIFFICULTY: easy";
		}
	@Override
	public void ControllerTouch (float mx, float my)
		{
		gameclass.Resume();
		Log.d("LinesGame", "Touch: "+mx+", "+my);
		if (difficulcy.onClick((int)mx, (int)my))
			{
			dialog.show();
			//gameclass.ChangeAct(0, 1);
			}
		}
	@Override
	public void Draw (Canvas canvas)
		{
		difficulcy.DrawThis(canvas);
		//canvas.drawText("This is options menu", 10, scrw/10+10, text_paint);
		gameclass.Pause();
		}
    @Override
    public void onClick(DialogInterface dialog, int which)
    	{
    	gameclass.Resume();
    	Log.d("LinesGame", "which = "+which);
    	switch(which)
    		{
    		case 0: { difficulcy.text = "DIFFICULCY: easy";   gameclass.gamehardness = Defs.HEASY; break; }
    		case 1: { difficulcy.text = "DIFFICULCY: medium"; gameclass.gamehardness = Defs.HMEDIUM; break; }
    		case 2: { difficulcy.text = "DIFFICULCY: hard";   gameclass.gamehardness = Defs.HHARD; break; }
    		default: { break; }
    		}
        dialog.dismiss();			
    	}
	}
