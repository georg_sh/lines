package game.lazystudio.lines;

import java.util.Arrays;
import java.util.Stack;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.LightingColorFilter;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.util.Log;

public class MainGame extends MyActivity
{
LinesGame appclass;
GameObjects gameclass;
public float scrw = 0;
public float scrh = 0;
public float cellsize = 0;
public int ballr = 0;
GameCell viewdesk[][];
int lastop = 0;
int ballx = 0;
int bally = 0;
int wayx[];
int wayy[];
int state = Defs.ST_FREE;
int hardness = Defs.HEASY;
Paint colors[];
ColorFilter filters[];
boolean endgame = false;
int bonus = 0;
int wsize = 0;
int score = 0;
int wayopacy = 0;
Bitmap sphere;
Bitmap csphere;

Paint text_paint;

public MainGame(LinesGame appclass, GameObjects gameclass, int scrw, int scrh)
	{
	colors  = new Paint      [Defs.HARDNESS+2];
	filters = new ColorFilter[Defs.HARDNESS+2];
	colors[0] = null;
	colors[1] = new Paint();
	filters[1] = new LightingColorFilter(Color.RED, 1);
	colors[1].setColorFilter(filters[1]);
	colors[2] = new Paint();
	filters[2] = new LightingColorFilter(Color.rgb(25, 200, 50), 1);
	colors[2].setColorFilter(filters[2]);
	colors[3] = new Paint();
	filters[3] = new LightingColorFilter(Color.rgb(60,90,255), 1);
	colors[3].setColorFilter(filters[3]);
	colors[4] = new Paint();
	filters[4] = new LightingColorFilter(Color.rgb(255,255,0), 1);
	colors[4].setColorFilter(filters[4]);
	colors[5] = new Paint();
	filters[5] = new LightingColorFilter(Color.rgb(0,255,255), 1);
	colors[5].setColorFilter(filters[5]);
	colors[6] = new Paint();
	filters[6] = new LightingColorFilter(Color.rgb(255,100,255), 1);
	colors[6].setColorFilter(filters[6]);
	colors[7] = new Paint();
	filters[7] = new LightingColorFilter(Color.rgb(255,255,255), 1);
	colors[7].setColorFilter(filters[7]);
	this.scrw = scrw;
	this.scrh = scrh;
	this.appclass = appclass;
	this.gameclass = gameclass;
	text_paint = new Paint();
	text_paint.setColor(Color.WHITE);
	text_paint.setStyle(Style.FILL);
	text_paint.setTextSize(scrw/10);
	cellsize = scrw/Defs.DESKSIZE;
	//btnt_paint = new Paint();
	//btnt_paint.setColor(Color.WHITE);
	//btnt_paint.setStyle(Style.FILL);
	//btnt_paint.setTextSize(scrw/10);
	ballr = (int)(cellsize/2-2);
	Resources res = appclass.getResources();
	BitmapFactory.Options options = new BitmapFactory.Options();
	options.inScaled = false;
	sphere = BitmapFactory.decodeResource(res, R.drawable.sphere, options);
	sphere = Bitmap.createScaledBitmap(sphere, (int)ballr*2, (int)ballr*2, false);
	csphere = BitmapFactory.decodeResource(res, R.drawable.spherec, options);
	csphere = Bitmap.createScaledBitmap(csphere, (int)ballr*2, (int)ballr*2, false);
	//String start_t = "NEW GAME";
	//int tw = (int)btnt_paint.measureText(start_t);
	
	//btn_bmp = BitmapFactory.decodeResource(res, R.drawable.ui_pattern, options);
	//btn_bmp = Bitmap.createScaledBitmap(btn_bmp, tw+20, (int)scrw/10, false);
	
	//start_game = new UIGameButton(btn_bmp, scrw/2, scrh/2, start_t, btnt_paint);
	
	viewdesk = new GameCell[Defs.DESKSIZE][Defs.DESKSIZE];
	wayx = new int[Defs.DESKSIZE*Defs.DESKSIZE];
	wayy = new int[Defs.DESKSIZE*Defs.DESKSIZE];
	for (int x = 0; x < Defs.DESKSIZE; x++)
	for (int y = 0; y < Defs.DESKSIZE; y++)
		{
		viewdesk[x][y]=new GameCell(ballr);
		}
	AddRandomBalls();
	}
void InitNewGame()
{
hardness = gameclass.gamehardness;
score = 0;
wsize = 0;
wayopacy = 0;
ballx = 0;
endgame = false;
bally = 0;
state = Defs.ST_FREE;
text_paint.setColor(Color.WHITE);
text_paint.setStyle(Style.FILL);
text_paint.setTextSize(scrw/10);
for (int x = 0; x < Defs.DESKSIZE; x++)
for (int y = 0; y < Defs.DESKSIZE; y++)
	{
	viewdesk[x][y]=new GameCell(ballr);
	}
AddRandomBalls();
}
public boolean findway (int ax, int ay, int bx, int by)
{
int deskway[][] = new int[Defs.DESKSIZE][Defs.DESKSIZE];
for (int i = 0; i < Defs.DESKSIZE; i++)
Arrays.fill(deskway[i], 0);
int dx[] = new int[4];
dx[0] = 1;
dx[1] = 0;
dx[2] = -1;
dx[3] = 0;
int dy[] = new int[4];
dy[0] = 0;
dy[1] = 1;
dy[2] = 0;
dy[3] = -1;
int d=1, x=0, y=0, k=0;
deskway[ax][ay] = 1;
boolean stop = true;
do
    {
    stop = true;
    for (x = 0; x < Defs.DESKSIZE; x++)
    for (y = 0; y < Defs.DESKSIZE; y++)
    if (deskway[x][y] == d)
        {
        for (k = 0; k < 4; k++)
            {
            if (y + dy[k] < 0 || y + dy[k] >= Defs.DESKSIZE) continue;
            if (x + dx[k] < 0 || x + dx[k] >= Defs.DESKSIZE) continue;
            if (deskway[x + dx[k]][y + dy[k]] == 0 &&
                viewdesk[x + dx[k]][y + dy[k]].realstate == 0)
                {
                stop = false;
                deskway[x + dx[k]][y + dy[k]] = d + 1;
                }
            }
        }
    d++;
    }
while (!stop && deskway[bx][by] == 0);
if (deskway[bx][by] == 0) { Log.d("LinesGame", "findway ret false"); return false; }

wsize = deskway[bx][by];
x = bx;
y = by;
d = deskway[bx][by];
while (d > 1)
    {
    wayx[d-1] = x;
    wayy[d-1] = y;
    d--;
    for (k = 0; k < 4; ++k)
	    {
        if (y + dy[k] < 0 || y + dy[k] >= Defs.DESKSIZE) continue;
        if (x + dx[k] < 0 || x + dx[k] >= Defs.DESKSIZE) continue;
	    if (deskway[x + dx[k]][y + dy[k]] == d)
	        {
	        x = x + dx[k];
	        y = y + dy[k];
	        break;
	        }
	    }
    }
wayx[0] = ax;
wayy[0] = ay;
Log.d("LinesGame", "findway ret true");
return true;
}
public void ControllerMove (float dx, float dy, float size)
{
//Log.d("LinesGame", "Move: "+dx+", "+dy);
}

public void ControllerTouch (float mx, float my)
{
Log.d("MainGame", "Touch: "+mx+", "+my);
if (state == Defs.ST_FREE)
{
int tmpballx = ((int)mx)/(int)cellsize;
int tmpbally = ((int)my)/(int)cellsize;
if (tmpballx < Defs.DESKSIZE && tmpbally < Defs.DESKSIZE && viewdesk[tmpballx][tmpbally].realstate != 0)
    {
	gameclass.Resume();
	state = Defs.ST_KEEPBALL;
    ballx = tmpballx;
    bally = tmpbally;

    KeepCell(ballx, bally);
    }
}
else if(state == Defs.ST_KEEPBALL)
{
gameclass.Resume();
int px = ((int)mx)/(int)cellsize;
int py = ((int)my)/(int)cellsize;
if (px < Defs.DESKSIZE && py < Defs.DESKSIZE)
{
if (viewdesk[px][py].realstate != 0)
    {
    viewdesk[ballx][bally].state = 0;
    viewdesk[ballx][bally].radius = ballr;
    KeepCell(px, py);
    ballx = px; bally = py;
    }
else
if (findway (ballx, bally, px, py))
    {
	lastop = viewdesk[ballx][bally].realstate;
	wayopacy = Defs.LINESH;
    SetCell(px, py, viewdesk[ballx][bally].realstate);
    SetCell(ballx, bally, 0);
    int fscore = score;
    score += CheckAndDeleteAllLines();
    
    if (fscore == score)
        {
        int freecells = AddRandomBalls();
        if(freecells < 0) { EndGame(); return; }
        score += CheckAndDeleteAllLines();
        if (freecells == 0 && fscore == score) { EndGame(); return; }
        }
    state = Defs.ST_FREE;
    }
else { ; }
}
else
    {
    ;
    }
}
}
int CheckAndDeleteFives (GameCell array[], int len, Stack<GameCell> todelete)
{
int sum = 0;
int counter = 0;
int colorballs = 0;
int colour = array[0].realstate;
//String s = "";
for (int i = 0; i < len; i++)
	{
	//s += array[i].realstate;
	//s += "("+counter+","+colorballs+")";
	if (array[i].realstate == Defs.HARDNESS+1)
		{
		counter++;
		colorballs++;
		continue;
		}
	if (array[i].realstate == 0)
		{
		colorballs = 0;
		if (counter >= 5)
			{
			if (counter > 5) { bonus += counter-5+2; }
			for (int k = 0; k < counter; k++)
				{
				todelete.push(array[i-k-1]);
				}
			sum += counter;
			}
		counter = 0;
		continue;
		}
	if (array[i].realstate == colour)
		{
		colorballs = 0;
		counter++;
		continue;
		}
	// array[i].realstate != colour != 0 != couloured
	if (counter >= 5)
		{
		if (counter > 5) { bonus += counter-5+2; }
		for (int k = 0; k < counter; k++)
			{
			todelete.push(array[i-k-1]);
			}
		sum += counter;
		counter = 1;
		}
	colour = array[i].realstate;
	counter = colorballs+1;
	colorballs = 0;
	}
if (counter >= 5)
	{
	if (counter > 5) { bonus += counter-5+2; }
	for (int k = 0; k < counter; k++)
		{
		todelete.push(array[len-1-k]);
		}
	sum += counter;
	counter = 0;
	}
//Log.d("LinesGame", s);
return sum;
}
int CheckAndDeleteFiveDiag45(GameCell array[], Stack<GameCell> todelete)
	{
	int sum = 0;
	for (int diag = 4; diag < Defs.DESKSIZE*2-5; diag++)
		{
		int sx = 0;
		int sy = Defs.DESKSIZE-diag-1;
		if (diag >= Defs.DESKSIZE)
		    {
		    sx = diag-Defs.DESKSIZE+1;
		    sy = 0;
		    }
		int i = 0;
		for (; sx+i < Defs.DESKSIZE && sy+i < Defs.DESKSIZE; i++)
		    {
		    int curx = sx+i;
		    int cury = sy+i;
		    array[i] = viewdesk[curx][cury];
		    }
		sum += CheckAndDeleteFives (array, i, todelete);
		}
	return sum;
	}
int CheckAndDeleteFiveDiag135(GameCell array[], Stack<GameCell> todelete)
	{
	int sum = 0;
	for (int diag = 4; diag < Defs.DESKSIZE*2-5; diag++)
		{
		int sx = diag;
		int sy = 0;
		if (diag >= Defs.DESKSIZE)
		    {
		    sx = Defs.DESKSIZE-1;
		    sy = diag-Defs.DESKSIZE+1;
		    }
		int i = 0;
		for (; sx-i >= 0 && sy+i < Defs.DESKSIZE; i++)
		    {
		    int curx = sx-i;
		    int cury = sy+i;
			array[i] = viewdesk[curx][cury];
		    }
		sum += CheckAndDeleteFives (array, i, todelete);
		}
	return sum;
	}
int CheckAndDeleteFiveRowsAndCols(GameCell array[], Stack<GameCell> todelete)
{
int sum = 0;
for (int x = 0; x < Defs.DESKSIZE; x++)
	{
	for (int y = 0; y < Defs.DESKSIZE; y++) { array[y] = viewdesk[x][y]; }
	sum += CheckAndDeleteFives (array, Defs.DESKSIZE, todelete);
	for (int y = 0; y < Defs.DESKSIZE; y++) { array[y] = viewdesk[y][x]; }
	sum += CheckAndDeleteFives (array, Defs.DESKSIZE, todelete);
	}
return sum;
}
public int CheckAndDeleteAllLines()
	{
	int score = 0;
	
	GameCell array[] = new GameCell[Defs.DESKSIZE];
	Stack<GameCell> todelete = new Stack<GameCell>();
	
	score += CheckAndDeleteFiveRowsAndCols(array, todelete);
	score += CheckAndDeleteFiveDiag45(array, todelete);
	score += CheckAndDeleteFiveDiag135(array, todelete);
	while (!todelete.isEmpty())
		{
		RemCell(todelete.pop());
		}
	return score;
	}
public void KeepCell (int x, int y)
	{
	viewdesk[x][y].state = Defs.CHSIZE;
	viewdesk[x][y].albl = 100;
	viewdesk[x][y].opacity = 100;
	}
public boolean SetCell (int x, int y, int color)
	{
	viewdesk[x][y].realstate = color;
	if (color == 0)
	    {
	    viewdesk[x][y].state = Defs.REMCOLOR;
	    viewdesk[x][y].opacity = 100;
	    viewdesk[x][y].radius = ballr;
	    return true;
	    }
	viewdesk[x][y].bcolor = colors[color];
	viewdesk[x][y].state = Defs.CHCOLOR;
	viewdesk[x][y].radius = ballr;
	viewdesk[x][y].opacity = 0;
	return true;
	}
public void RemCell (GameCell cell)
	{
	cell.realstate = 0;
	cell.state = Defs.REMBALL;
	cell.albl = 100;
	cell.opacity = 100;
	cell.radius = ballr;
	}
int AddRandomBalls ()
{
int freecells = 0;
for (int x = 0; x < Defs.DESKSIZE; x++)
for (int y = 0; y < Defs.DESKSIZE; y++)
{
if (viewdesk[x][y].realstate == 0) freecells++;
}
if (freecells < Defs.BALLSPERMOVE) { return freecells-Defs.BALLSPERMOVE; }
int balls[] = new int[Defs.BALLSPERMOVE];
int places[] = new int[Defs.BALLSPERMOVE];
Arrays.fill(balls, 0);
Arrays.fill(places, 0);
for (int i = 0; i < Defs.BALLSPERMOVE; i++)
{
places[i]=(int)(Math.random() * (freecells));
boolean placesdiff = false;
while (!placesdiff)
    {
    placesdiff = true;
    for (int k = 0; k < i; k++)
        {
        if (places[i] == places[k])
            {
            places[i]=(places[i]+1)%freecells;
            placesdiff = false;
            break;
            }
        }
    }
}
for (int i = 0; i < Defs.BALLSPERMOVE; i++)
	{
	balls[i] = (int)(Math.random() * (hardness))+1;
	if (bonus > 0) { balls[i] = Defs.HARDNESS+1; bonus--; }
	}
freecells = 0;
for (int x = 0; x < Defs.DESKSIZE; x++)
for (int y = 0; y < Defs.DESKSIZE; y++)
{
if (viewdesk[x][y].realstate == 0) freecells++;
else { continue; }
for (int i = 0; i < Defs.BALLSPERMOVE; i++)
    {
    if (freecells-1 == places[i]) { SetCell(x, y, balls[i]); }
    }
}
return freecells-Defs.BALLSPERMOVE;
}
public boolean UpdateViewDesk()
{
boolean changed = false;
for (int x = 0; x < Defs.DESKSIZE; x++)
for (int y = 0; y < Defs.DESKSIZE; y++)
{
if (viewdesk[x][y].bcolor == null) { continue; }
switch (viewdesk[x][y].state)
    {
    case Defs.REMCOLOR:
        {
        changed = true;
        if (viewdesk[x][y].opacity <= 0)
            {
            viewdesk[x][y].bcolor = null;
            viewdesk[x][y].state = 0;
            break;
            }
        viewdesk[x][y].opacity -= 5;
        break;
        }
    case Defs.REMBALL:
        {
        changed = true;
        if (viewdesk[x][y].opacity <= 0)
            {
            viewdesk[x][y].bcolor = null;
            viewdesk[x][y].state = 0;
            break;
            }
        viewdesk[x][y].radius *= 1.05;
        viewdesk[x][y].opacity -= 5;
        break;
        }
    case Defs.CHCOLOR:
        {
        changed = true;
        if (viewdesk[x][y].opacity >= 100)
            {
            viewdesk[x][y].state = 0;
            break;
            }
        viewdesk[x][y].opacity += 5;
        break;
        }
    case Defs.CHSIZE:
        {
        changed = true;
        //printf("chsize %d\n", viewdesk[x][y].albl);
        viewdesk[x][y].radius=ballr*(Math.abs(viewdesk[x][y].albl-50)+50)/100;
        viewdesk[x][y].albl += 2;
        if (viewdesk[x][y].albl > 100) { viewdesk[x][y].albl = 0; }
        break;
        }
    default: { break; }
    }
}
return changed;
}
public void printdesk (Canvas canvas)
{
Paint linep = new Paint();
Paint alphap = new Paint();
linep.setColor(Color.WHITE);
for (int x = 0; x < Defs.DESKSIZE; x++)
    {
	canvas.drawLine(0, (x+1)*cellsize, cellsize*Defs.DESKSIZE, (x+1)*cellsize, linep);
	canvas.drawLine((x+1)*cellsize, 0, (x+1)*cellsize, cellsize*Defs.DESKSIZE, linep);
    for (int y = 0; y < Defs.DESKSIZE; y++)
        {
        if(viewdesk[x][y].bcolor == null) { continue; }
        Bitmap bmp = sphere;
        if (viewdesk[x][y].bcolor == colors[Defs.HARDNESS+1])
        	{
        	bmp = csphere;
        	}
        int opac = viewdesk[x][y].opacity;
        float size = viewdesk[x][y].radius;
        if (opac != 100 || size != ballr)
        	{
        	alphap.setColorFilter(viewdesk[x][y].bcolor.getColorFilter());
        	alphap.setAlpha(255*opac/100);
        	viewdesk[x][y].DrawThisEx(canvas, bmp, x*cellsize+cellsize/2, y*cellsize+cellsize/2, alphap);
        	continue;
        	}
        viewdesk[x][y].DrawThis(canvas, bmp, x*cellsize+cellsize/2, y*cellsize+cellsize/2);
        }
    }
}
void PrintWay(Canvas canvas)
{
Paint linep = new Paint();
linep.setColor(Color.WHITE);
linep.setColorFilter(filters[lastop]);
linep.setAlpha(255*wayopacy/Defs.LINESH);
for (int i = 0; i < wsize-1; i++)
    {
	//if (viewdesk[wayx[i]][wayy[i]].realstate != 0) { continue; }
	/*float dx = wsize-i;
	float dw = Defs.LINESH-wayopacy;
	dx /= wsize;
	dw = 1-dw/Defs.LINESH;
	float alph = 0;
	alph = 255-255*Math.abs(dw-dx);
	linep.setAlpha((int)alph);
	canvas.drawBitmap(sphere, wayx[i]*cellsize+cellsize/2-ballr, wayy[i]*cellsize+cellsize/2-ballr, linep);*/
	
	canvas.drawLine(wayx[i]*cellsize+cellsize/2,
					wayy[i]*cellsize+cellsize/2,
					wayx[i+1]*cellsize+cellsize/2,
					wayy[i+1]*cellsize+cellsize/2, linep);
    }
}
public boolean UpdateAll()
{
return UpdateViewDesk();
}
public void DrawText(Canvas canvas)
{
canvas.drawText("score: "+score, 15, scrw+scrw/10, text_paint); 
}
public void EndGame()
{
endgame = true;
text_paint.setTextSize(scrw/10);
text_paint.setColor(Color.WHITE);
wayopacy = 100;
}
@Override
public void Resume(int param)
	{
	if (param == 1) { InitNewGame(); }
	}
public void Draw (Canvas canvas)
	{
	if (endgame == true)
		{
		canvas.drawText("Game Over", 20, 20+scrw/10, text_paint); 
		canvas.drawText("total score: "+score, 20, 20+scrw/10+scrw/10, text_paint); 
		if (wayopacy == 0) { gameclass.ChangeAct(1, 0); }
		wayopacy--;
		return;
		}
	boolean pause = false;
	if (!UpdateAll() && wayopacy <= 0) { pause = true; }
	printdesk(canvas);
	if (wayopacy > 0)
	    {
		//Log.d("LinesGame", "PrintWay");
	    PrintWay(canvas);
	    wayopacy--;
	    }
	DrawText(canvas);
	if(pause) { gameclass.Pause(); }
	}
}
