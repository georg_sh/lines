package game.lazystudio.lines;

public final class Defs
	{
	final static int CHCOLOR = 1;
	final static int CHSIZE = 2;
	final static int REMCOLOR = 4;
	final static int REMBALL = 8;
	final static int ST_FREE = 0;
	final static int ST_KEEPBALL = 1;
	final static int DESKSIZE = 9;
	final static int HEASY = 4;
	final static int HMEDIUM = 5;
	final static int HHARD = 6;
	final static int HARDNESS = 6;
	final static int BALLSPERMOVE = 4;
	final static int LINESH = 100;
	}
