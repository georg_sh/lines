package game.lazystudio.lines;

import android.graphics.Canvas;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.util.Log;

public class Controller implements SensorEventListener
{
	private float controller_x;
	private float controller_y;
	private boolean movement = true;
	private boolean contr_visible = false;
	private GameObjects game;

Controller (GameObjects game)
		{
		this.game = game;
		}
public boolean OnBack()
	{
	return game.Back();
	}
public void OnDown(float x, float y)
	{
	Log.d("LightGame", "touch down: "+x+", "+y);
	movement = false;
	controller_x = x;
	controller_y = y;
	}
public void OnMove(float x, float y)
	{
	//Log.d("LightGame", "touch move: "+x+", "+y);
	float vx_temp = x-controller_x;
	float vy_temp = y-controller_y;
	float size = (float)Math.sqrt(vx_temp*vx_temp+vy_temp*vy_temp);
	if (size <= 20f) { game.ControllerMove(0, 0, size); return; }
	movement = true;
	contr_visible = true;
	game.ControllerMove(vx_temp, vy_temp, size);
	}
public void OnUp(float x, float y)
	{
	Log.d("LightGame", "touch up: "+x+", "+y);
	if (movement == false)
		{
		//Shoot();
		game.ControllerTouch(x, y);
		movement = true;
		return;
		}
	game.ControllerMove(0, 0, 1);
	if(contr_visible)
		{
		contr_visible = false;
		}
	}
	public void Draw (Canvas canvas)
		{
		//if (!contr_visible) { return; }
		//canvas.drawBitmap(contr_b, controller_x-radius, controller_y-radius, null);
		}
	@Override
	public void onSensorChanged(SensorEvent event) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {
		// TODO Auto-generated method stub
		
	}
}
