package game.lazystudio.lines;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Point;
import android.util.Log;
import android.view.Display;
import android.view.View;

public class LinesGame extends Application
{
private static DrawView gameview;
private static MainActivity gameactivity;
private static GameObjects gameobj;
private static LinesGame singleton;
private boolean startflag = false;
@Override
public void onCreate()
	{
	super.onCreate();
	singleton = this;
	
	//SensorManager accels = (SensorManager)getSystemService(SENSOR_SERVICE);
	//accels.registerListener(gameobj,
	//		accels.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
	//		SensorManager.SENSOR_DELAY_NORMAL);
	}

@SuppressWarnings("deprecation")
@SuppressLint("NewApi")
public void SetActivity (MainActivity gameact)
	{
	gameactivity = gameact;
	Display display = gameact.getWindowManager().getDefaultDisplay();
	int scrw = 0;
	int scrh = 0;
	if (android.os.Build.VERSION.SDK_INT < 13)
		{
		scrw = display.getWidth();
		scrh = display.getHeight();
		}
	else
		{
		Point size = new Point();
		display.getSize(size); // TODO Screen orientation detection
		scrw = size.x;
		scrh = size.y;
		}
	if (!startflag)
		{
		startflag = true;
		Log.d("LinesGame", "Create game: "+scrw+"x"+scrh);
		gameobj = new GameObjects(this, scrw, scrh);
		}
	else
		{
		// TODO Make resize method
		}
	
	gameview = new DrawView(gameactivity);
	gameview.setGame(gameobj);
	gameactivity.setContentView(gameview);
	}
public static MainActivity GetActivity() { return gameactivity; }
public static DrawView getGameView() { return gameview; }
public static GameObjects getGameObj() { return gameobj; }
public static LinesGame getInstance()
	{
	return singleton;
	}
class DrawView extends View
	{
	private GameObjects game = null;
	private boolean pause = false;
  	public DrawView(Context context)
    	{
  		super(context);
    	}
    public void setGame(GameObjects game)
    	{
    	this.game = game;
    	}
    public void Pause()  { Log.d("LinesGame", "Pause"); pause = true; }
    public void Resume() { Log.d("LinesGame", "Resume"); pause = false; invalidate(); }
    @Override
    protected void onDraw(Canvas canvas)
    	{
    	//Log.d("LightGame", "onDraw");
    	if(game != null)
    		{
    		game.Draw(canvas);
    		}
    	if(!pause) { invalidate(); }
    	}
	}

}
