package game.lazystudio.lines;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.util.Log;

public class MenuClass extends MyActivity
{
LinesGame appclass;
GameObjects gameclass;
public float scrw = 0;
public float scrh = 0;
Controller control;
MyActivity activs[];
Paint btnt_paint;
Bitmap btn_bmp;
UIGameButton start_game_b;
UIGameButton resume_b;
UIGameButton options_b;
UIGameButton help_b;
UIGameButton about_b;
boolean action = false;

public MenuClass(LinesGame appclass, GameObjects gameclass, int scrw, int scrh)
	{
	this.scrw = scrw;
	this.scrh = scrh;
	this.appclass = appclass;
	this.gameclass = gameclass;
	btnt_paint = new Paint();
	btnt_paint.setColor(Color.WHITE);
	btnt_paint.setStyle(Style.FILL);
	btnt_paint.setTextSize(scrw/7);
	//Resources res = appclass.getResources();
	BitmapFactory.Options options = new BitmapFactory.Options();
	options.inScaled = false;
	String start_t = "NEW GAME";
	String resume_t = "RESUME";
	String opt_t = "OPTIONS";
	String help_t = "HELP";
	String about_t = "ABOUT";
	
	int tw = (int)btnt_paint.measureText(start_t);
	tw = Math.max(tw, (int)btnt_paint.measureText(resume_t));
	tw = Math.max(tw, (int)btnt_paint.measureText(opt_t));
	tw = Math.max(tw, (int)btnt_paint.measureText(help_t));
	tw = Math.max(tw, (int)btnt_paint.measureText(about_t));
	
	//btn_bmp = BitmapFactory.decodeResource(res, R.drawable.ui_pattern2, options);
	int btnw = (int)((float)tw*1.3);
	int btnh = scrw/7+10;
	//btn_bmp = Bitmap.createScaledBitmap(btn_bmp, btnw, btnh, false);
	int btnh_bord = btnh+10;
	start_game_b = new UIGameButton(btnw, btnh, scrw/2, scrh/2-2*btnh_bord, start_t, btnt_paint);
	resume_b     = new UIGameButton(btnw, btnh, scrw/2, scrh/2-btnh_bord, resume_t, btnt_paint);
	options_b    = new UIGameButton(btnw, btnh, scrw/2, scrh/2, opt_t,   btnt_paint);
	help_b       = new UIGameButton(btnw, btnh, scrw/2, scrh/2+btnh_bord, help_t,  btnt_paint);
	about_b      = new UIGameButton(btnw, btnh, scrw/2, scrh/2+2*btnh_bord, about_t, btnt_paint);
	}
@Override
public void ControllerMove (float dx, float dy, float size)
	{
	//Log.d("LinesGame", "Move: "+dx+", "+dy);
	}
@Override
public void ControllerTouch (float mx, float my)
	{
	Log.d("LinesGame", "Touch: "+mx+", "+my);
	if (start_game_b.onClick((int)mx, (int)my))
		{
		action = true; gameclass.Resume();
		gameclass.ChangeAct(0, 1);
		}
	if (resume_b.onClick((int)mx, (int)my))
		{
		action = true; gameclass.Resume();
		gameclass.ChangeAct(0, 0);
		}
	if (options_b.onClick((int)mx, (int)my))
		{
		action = true; gameclass.Resume();
		gameclass.ChangeAct(2, 0);
		}
	}
@Override
public void Resume(int param)
	{
	action = false;
	}
public void Draw (Canvas canvas)
	{
	start_game_b.DrawThis(canvas);
	resume_b.DrawThis(canvas);
	options_b.DrawThis(canvas);
	help_b.DrawThis(canvas);
	about_b.DrawThis(canvas);
	if(!action) { gameclass.Pause(); }
	}
}